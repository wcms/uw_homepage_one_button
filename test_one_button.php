<?php

/**
 * @file
 * Example implementation.
 */

// VARIABLES USED IN APPLICATION.
//
// Initialization Vector, randomly generated and saved each time
// Note: This does not need to be kept secret.
$iv = substr(sha1(mt_rand()), 0, 16);

// Salt: this should be stored somewhere in database and retrieved, this should not be in the code.
$salt = '';

// Username that has to ability to post on the UWaterloo homepage, this should be retreived from a database and not stored in code.
$username = '';

// Password for the user that has the ability to post on the UWaterloo homepage, this should be retieved from a database and not stored in code.
$password = '';

// The URL for the alert POST, this should be stored in a database and not stored in code.
$post_alert_url = '';

// END VARIABLES USED IN APPLICATION.
//
// If the submit button is pushed on the form.
if (isset($_POST['alert_submit_button'])) {

  // Ensure that all fields are filled out.
  if ($_POST['alert_title'] == "" || $_POST['alert_body'] == "" || $_POST['alert_status'] == "") {
    $alert_message = 'Please ensure all fields are completed';
  }
  else {

    // Data to send to the POST.
    $data = array(
      'username' => openssl_encrypt($username, 'aes-256-cbc', $salt, NULL, $iv),
      'password' => openssl_encrypt($password, 'aes-256-cbc', $salt, NULL, $iv),
      'type' => $_POST['alert_type'],
      'title' => $_POST['alert_title'],
      'body' => $_POST['alert_body'],
      'sidebar' => $_POST['alert_sidebar'],
      'status' => $_POST['alert_status'],
      'iv' => $iv,
    );

    // cURL: Posting to the homepage page.
    $curl = curl_init();
    curl_setopt($curl, CURLOPT_URL, $post_alert_url);
    // Do a regular HTTP POST.
    curl_setopt($curl, CURLOPT_POST, 1);
    curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
    // Set POST data.
    curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($data));
    // Ask to not return Header.
    curl_setopt($curl, CURLOPT_HEADER, 0);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($curl, CURLOPT_FAILONERROR, 1);

    $response = curl_exec($curl);
    $http_code = curl_getinfo($curl, CURLINFO_HTTP_CODE);

    print_r($response);
    print_r($http_code);
  }
}

if (isset($alert_message)) {
  print $alert_message;
}

?>

<form id="alert_form" action="<?php echo $_SERVER['PHP_SELF'];?>" method="post">
    <b><u>Alerts</u></b>
    <br /><br />
    Type:<br />
    <select name="alert_type">
        <option value="alerts">Minor Alert</option>
        <option value="alerts_major">Major Alert</option>
    </select>
    <br /><br />
    Title:<br />
    <input type="text" name="alert_title" value="" />
    <br /><br />
    Body:<br />
    <textarea name="alert_body" cols="60" rows="10"></textarea>
    <br /><br />
    Sidebar:<br />
    <textarea name="alert_sidebar" cols="60" rows="10"></textarea>
    <br /><br />
    Status:<br />
    <select name="alert_status">
        <option value="1">Publish</option>
        <option value="0">Unpublish</option>
    </select>
    <br /><br />
    <input type="submit" name="alert_submit_button" value="Submit">
</form>

<br /><br />
